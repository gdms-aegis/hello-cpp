FROM quay.io/eboyer/builder-cpp:0.1.0 AS builder

COPY . /src
WORKDIR /src
RUN g++ main.cpp -o helloworld

FROM registry.redhat.io/ubi7/ubi-minimal
WORKDIR /opt/app-root
COPY --from=builder /src/helloworld ./
CMD ["./helloworld"]
